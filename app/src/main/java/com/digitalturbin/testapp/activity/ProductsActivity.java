package com.digitalturbin.testapp.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.digitalturbin.testapp.R;
import com.digitalturbin.testapp.base.view.BaseActivity;
import com.digitalturbin.testapp.modules.products.view.ProductsFragment;

/**
 * Created by sonika on 20/11/16.
 */

public class ProductsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
    }

    private void initializeFragment(){
            ProductsFragment productsFragment = new ProductsFragment();
            addFragment(R.id.fragmentContainer, productsFragment, ProductsFragment.class.getSimpleName(), false);
    }
}
