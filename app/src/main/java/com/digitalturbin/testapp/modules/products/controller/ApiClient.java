package com.digitalturbin.testapp.modules.products.controller;

import android.util.Log;

import com.digitalturbin.testapp.modules.products.model.Product;
import com.digitalturbin.testapp.modules.products.model.ProductListResponse;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by sonika on 21/11/16.
 */

public class ApiClient {

    private static final String TAG = ApiClient.class.getSimpleName();

    public static final String BASE_URL = "http://ads.appia.com/";
    private static Retrofit retrofit = null;
    public static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    private static OkHttpClient client;

    private static ApiClient apiClient;

    public static ApiClient getInstance(){

        if (apiClient == null)
            apiClient = new ApiClient();

        return apiClient;

    }

    private Retrofit getClient(){

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient().newBuilder().addInterceptor(interceptor).build();
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).
                    addConverterFactory(SimpleXmlConverterFactory.create()).client(client).build();

        }
        return retrofit;
    }

    public void getProductList(final ResponseListener responseListener){

        ApiInterface apiInterface = getClient().create(ApiInterface.class);

        Call<ProductListResponse> call = apiInterface.getProductsList("236","OVUJ1DJN","4288","4230","techtestsession","10","soni");

        call.enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                Log.d(TAG, "callApi:Response = "+response.body().toString());

                List<Product> productList = response.body().getAdsList();
                Log.d(TAG, "callApi:List Size = "+productList.size());

                responseListener.onSuccess(productList);

            }

            @Override
            public void onFailure(Call<ProductListResponse> call, Throwable throwable) {
                Log.d(TAG, "callApi:onFailure called : ");
                throwable.printStackTrace();
                responseListener.onFailure(throwable);
            }
        });

    }
}