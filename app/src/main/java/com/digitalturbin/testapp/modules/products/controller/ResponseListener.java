package com.digitalturbin.testapp.modules.products.controller;

import com.digitalturbin.testapp.modules.products.model.Product;

import java.util.List;

/**
 * Created by sonika on 21/11/16.
 */

public interface ResponseListener {

    void onSuccess(List<Product> productList);
    void onFailure(Throwable throwable);
}
