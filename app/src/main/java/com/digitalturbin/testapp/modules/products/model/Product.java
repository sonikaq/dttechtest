package com.digitalturbin.testapp.modules.products.model;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by sonika on 21/11/16.
 */

@Root(name = "ad")
public class Product implements Serializable{
    @Element(name = "appId", required = false)
    private String appId;
    @Element(name = "averageRatingImageURL", required = false)
    private String averageRatingImageURL;
    @Element(name = "bidRate", required = false)
    private float bidRate;
    @Element(name = "callToAction", required = false)
    private String callToAction;
    @Element(name = "campaignDisplayOrder", required = false)
    private int campaignDisplayOrder;
    @Element(name = "campaignId", required = false)
    private int campaignId;
    @Element(name = "campaignTypeId", required = false)
    private int campaignTypeId;
    @Element(name = "categoryName", required = false)
    private String categoryName;
    @Element(name = "clickProxyURL", required = false)
    private String clickProxyURL;
    @Element(name = "creativeId", required = false)
    private String creativeId;
    @Element(name = "homeScreen", required = false)
    private boolean homeScreen;
    @Element(name = "impressionTrackingURL", required = false)
    private String impressionTrackingURL;
    @Element(name = "isRandomPick", required = false)
    private boolean isRandomPick;
    @Element(name = "minOSVersion", required = false)
    private String minOSVersion;
    @Element(name = "numberOfRatings", required = false)
    private String numberOfRatings;
    @Element(name = "productDescription", required = false)
    private String productDescription;
    @Element(name = "productId", required = false)
    private String productId;
    @Element(name = "productName", required = false)
    private String productName;
    @Element(name = "productThumbnail", required = false)
    private String productThumbnail;
    @Element(name = "rating", required = false)
    private float rating;

    public String getAppId() {
        return appId;
    }

    public String getAverageRatingImageURL() {
        return averageRatingImageURL;
    }

    public float getBidRate() {
        return bidRate;
    }

    public String getCallToAction() {
        return callToAction;
    }

    public int getCampaignDisplayOrder() {
        return campaignDisplayOrder;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public int getCampaignTypeId() {
        return campaignTypeId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getClickProxyURL() {
        return clickProxyURL;
    }

    public String getCreativeId() {
        return creativeId;
    }

    public boolean isHomeScreen() {
        return homeScreen;
    }

    public String getImpressionTrackingURL() {
        return impressionTrackingURL;
    }

    public boolean isRandomPick() {
        return isRandomPick;
    }

    public String getMinOSVersion() {
        return minOSVersion;
    }

    public String getNumberOfRatings() {
        return numberOfRatings;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public String getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductThumbnail() {
        return productThumbnail;
    }

    public float getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "Product{" +
                "appId='" + appId + '\'' +
                ", averageRatingImageURL='" + averageRatingImageURL + '\'' +
                ", bidRate=" + bidRate +
                ", callToAction='" + callToAction + '\'' +
                ", campaignDisplayOrder=" + campaignDisplayOrder +
                ", campaignId=" + campaignId +
                ", campaignTypeId=" + campaignTypeId +
                ", categoryName='" + categoryName + '\'' +
                ", clickProxyURL='" + clickProxyURL + '\'' +
                ", creativeId='" + creativeId + '\'' +
                ", homeScreen=" + homeScreen +
                ", impressionTrackingURL='" + impressionTrackingURL + '\'' +
                ", isRandomPick=" + isRandomPick +
                ", minOSVersion='" + minOSVersion + '\'' +
                ", numberOfRatings='" + numberOfRatings + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", productId='" + productId + '\'' +
                ", productName='" + productName + '\'' +
                ", productThumbnail='" + productThumbnail + '\'' +
                ", rating=" + rating +
                '}';
    }
}
