package com.digitalturbin.testapp.modules.products.controller;

import com.digitalturbin.testapp.modules.products.model.ProductListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sonika on 21/11/16.
 */

public interface ApiInterface {

    @GET("getAds")
    //getAds?id=236&password=OVUJ1DJN&siteId=4288&deviceId=4230&sessionId=techtestsession&totalCampaignsRequested=10
    Call<ProductListResponse> getProductsList(@Query("id") String id,
                                              @Query("password") String password,
                                              @Query("siteId") String siteId,
                                              @Query("deviceId") String deviceId,
                                              @Query("sessionId") String sessionId,
                                              @Query("totalCampaignsRequested") String totalCampaignsRequested,
                                              @Query("lname") String lastName);
}
