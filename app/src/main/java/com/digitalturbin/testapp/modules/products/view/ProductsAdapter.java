package com.digitalturbin.testapp.modules.products.view;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalturbin.testapp.R;
import com.digitalturbin.testapp.base.view.BaseActivity;
import com.digitalturbin.testapp.constants.BundleKeys;
import com.digitalturbin.testapp.modules.products.model.Product;
import com.digitalturbin.testapp.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sonika on 20/11/16.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private static final String TAG = ProductsAdapter.class.getSimpleName();

    private ProductsFragment productsFragment;
    private List<Product> productList;

    public ProductsAdapter(ProductsFragment productsFragment, List<Product> productList) {
        this.productsFragment = productsFragment;
        this.productList = productList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productThumbNail)
        ImageView productThumbNail;

        @BindView(R.id.productName)
        TextView productName;

        @BindView(R.id.productRating)
        RatingBar productRating;

        @BindView(R.id.productRow)
        LinearLayout productRow;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.products_row, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductsAdapter.ViewHolder holder, final int position) {

//        Log.d(TAG, "List Size = "+productList.size()+" Position ="+position+" "+productList.get(position).getProductThumbnail());

        holder.productRow.setTag(position);

        /*Picasso.with(productsFragment.getActivity())
                .load(productList.get(position).getProductThumbnail())
                .placeholder(R.mipmap.ic_launcher) // optional
                .error(R.mipmap.ic_launcher)         // optional
                .into(holder.productThumbNail);*/

        Utilities.loadImage(productsFragment.getActivity(), productList.get(position).getProductThumbnail(), holder.productThumbNail);

        holder.productName.setText(productList.get(position).getProductName());
        holder.productRating.setRating(productList.get(position).getRating());

        holder.productRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick((int)holder.productRow.getTag());
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    void itemClick(int position){
        diplayDetailFragment(position);
    }

    private void diplayDetailFragment(int position){
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleKeys.PRODUCT_KEY, productList.get(position));
        productDetailFragment.setArguments(bundle);

        ((BaseActivity)productsFragment.getActivity()).addFragment(R.id.fragmentContainer, productDetailFragment, ProductDetailFragment.class.getSimpleName(), true);
    }


}
