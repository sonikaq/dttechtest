package com.digitalturbin.testapp.modules.products.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitalturbin.testapp.R;
import com.digitalturbin.testapp.modules.products.controller.ApiClient;
import com.digitalturbin.testapp.modules.products.controller.ResponseListener;
import com.digitalturbin.testapp.modules.products.model.Product;
import com.digitalturbin.testapp.utils.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sonika on 20/11/16.
 */

public class ProductsFragment extends Fragment {

    private static final String TAG = ProductsFragment.class.getSimpleName();

    @BindView(R.id.productsListView)
    RecyclerView productsListView;

    private ProductsAdapter productsAdapter;

    private List<Product> productsList;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_products, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        createProgressBar();

        if (!Utilities.isNetworkAvailable(getActivity())){
            showToast(getResources().getString(R.string.internet_connectivity_string));
            getActivity().finish();
            return;
        }

        ApiClient.getInstance().getProductList(new ResponseListener() {
            @Override
            public void onSuccess(List<Product> productList) {
                productsList = productList;
                initComponents();
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Throwable throwable) {

                showToast(getResources().getString(R.string.failure_response));
                progressDialog.dismiss();
            }
        });
    }

    private void initComponents(){
        productsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        Log.d(TAG, "List Size = "+productsList.size());
        productsAdapter = new ProductsAdapter(ProductsFragment.this, productsList);

        productsListView.setAdapter(productsAdapter);
    }

    private void createProgressBar(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.loading_string));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void showToast(String messageText){
        Toast.makeText(getActivity(), messageText, Toast.LENGTH_SHORT).show();
    }
}
