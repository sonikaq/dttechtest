package com.digitalturbin.testapp.modules.products.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonika on 21/11/16.
 */

@Root(name = "ads")
public class ProductListResponse {
    @ElementList(name = "ads", inline = true, required = false)
    private ArrayList<Product> adsList;

    @Element(name = "responseTime")
    private String responseTime;

    @Element(name = "serverId")
    private String serverId;

    @Element(name = "totalCampaignsRequested")
    private String totalCampaignsRequested;

    @Element(name = "version")
    private String version;

    public ArrayList<Product> getAdsList() {
        return adsList;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public String getServerId() {
        return serverId;
    }

    public String getTotalCampaignsRequested() {
        return totalCampaignsRequested;
    }

    public String getVersion() {
        return version;
    }
}
