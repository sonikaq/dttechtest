package com.digitalturbin.testapp.modules.products.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalturbin.testapp.R;
import com.digitalturbin.testapp.constants.BundleKeys;
import com.digitalturbin.testapp.modules.products.model.Product;
import com.digitalturbin.testapp.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sonika on 21/11/16.
 */

public class ProductDetailFragment extends Fragment {

    @BindView(R.id.adsImage)
    ImageView adImageView;

    @BindView(R.id.adName)
    TextView adNameTextView;

    @BindView(R.id.adDescription)
    TextView adDescriptionTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle bundle) {
        super.onActivityCreated(bundle);

        bundle = getArguments();

        Product product = (Product) bundle.getSerializable(BundleKeys.PRODUCT_KEY);

        Utilities.loadImage(getActivity(), product.getProductThumbnail(), adImageView);

        adNameTextView.setText(product.getProductName());
        adDescriptionTextView.setText(product.getProductDescription());
    }
}
